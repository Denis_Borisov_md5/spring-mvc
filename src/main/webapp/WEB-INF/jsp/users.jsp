<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>

    <head>
        <title>Users</title>
    </head>

    <body>
        <div>
            <table>
                <tr>
                    <td><p>id</p></td>
                    <td><p>login</p></td>
                    <td><p>password</p></td>
                    <td><p>email</p></td>
                </tr>
            <c:forEach var="user" items="${users}">
     	        <tr>
     	            <td><p> ${user.userId}</p></td>
     	            <td><p> ${user.login}</p></td>
     	            <td><p> ${user.password}</p></td>
     	            <td><p> ${user.email}</p></td>
     	        </tr>
            </c:forEach>
            </table>
        </div>
    </body>

</html>