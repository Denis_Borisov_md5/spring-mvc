<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>

<html>

    <head>
        <title>Home</title>
    </head>

    <body>
        <div>
            <p><h1>Home page</h1></p>
            <p>Hello <b>${login}</b> Wellcome to the Products Shop<p>
                <h3><a href="/products">Products</a></h3>
                <h3><a href="/logout">Logout</a></h3>
                <h3><a href="/users">Users</a></h3>
                <h3><a href="/search">Search</a></h3>
        </div>
    </body>

</html>