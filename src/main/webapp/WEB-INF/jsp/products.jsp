<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>

<html>

    <head>
        <title>Products</title>
        <script src="js/jquery-3.4.1.min.js"></script>
        <script src="js/ajax.js"></script>
    </head>

    <body>
        <div>
            <table id="productTable">
                <thread>
                    <tr>
                        <th><p>Id</p></th>
                        <th><p>Brand</p></th>
                        <th><p>Name</p></th>
                        <th><p>Cost</p></th>
                        <th><p>Delete</p></th>
                        <th><p>Update</p></th>
                    </tr>
                </thread>
                <tbody>
                    <c:forEach var="product" items="${products}">
                        <tr>
                            <td><p> ${product.productId}</p></td>
                            <td><p> ${product.brand}</p></td>
                            <td><p> ${product.name}</p></td>
                            <td><p> ${product.cost}</p></td>
                            <td><a href="/delete-product/${product.productId}">Delete</a></td>
                            <td><a href="/update-product/${product.productId}">Update</a></td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <br>
            <c:url value="/products" var="prev">
                <c:param name="page" value="${page-1}"/>
            </c:url>
            <c:if test="${page > 1}">
                <a href="<c:out value="${prev}" />">Prev</a>
            </c:if>
            <c:url value="/products" var="next">
                <c:param name="page" value="${page + 1}"/>
            </c:url>
            <c:if test="${page + 1 <= maxPages}">
                <a href='<c:out value="${next}" />'>Next</a>
            </c:if>
            <br>
            <a href='/create-product'>Create product</a>
        </div>
    </body>

</html>