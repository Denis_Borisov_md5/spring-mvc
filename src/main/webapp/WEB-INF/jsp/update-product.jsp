<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Product</title>
    </head>
    <body>
        <div>
            <h3>Update product</h3>
            <form action="/update-product" method="post">
                <div>
                    <label for="productId">Id: </label>
                    <input type="text" value="${product.productId}" name="productId" required>
                </div>
                <div>
                    <label for="brand">Brand: </label>
                    <input type="text" value="${product.brand}" name="brand" required>
                </div>
                <div>
                    <label for="name">Name: </label>
                    <input type="text" value="${product.name}" name="name" required>
                </div>
                <div>
                    <label for="cost">Cost: </label>
                    <input type="text" value="${product.cost}" name="cost" required>
                </div>
                <button type="submit">Upd</button>
            </form>
            <a href='/products'>Cancel</a>
            <a href='/home'>Home</a>
        </div>
    </body>
</html>