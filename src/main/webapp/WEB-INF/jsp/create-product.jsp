<%@ page contentType="text/html; charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Product</title>
    </head>
    <body>
        <div>
            <h3>Create product</h3>
            <form action="/create-product" method="post">
                <div>
                    <label for="brand">Brand: </label>
                    <input type="text" value="${product.brand}" name="brand" required>
                </div>
                <div>
                    <label for="name">Name: </label>
                    <input type="text" value="${product.name}" name="name" required>
                </div>
                <div>
                    <label for="cost">Cost: </label>
                    <input type="text" value="${product.cost}" name="cost" required>
                </div>
                <button type="submit">Save</button>
            </form>
            <a href='/products'>Cancel</a>
            <a href='/home'>Home</a>
        </div>
    </body>
</html>