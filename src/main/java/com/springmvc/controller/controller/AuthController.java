package com.springmvc.controller.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class AuthController {

    @GetMapping("/")
    private ModelAndView defaults(){
        return new ModelAndView("redirect:login");
    }

    @GetMapping("/home")
    private ModelAndView home(HttpSession session){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("login", session.getAttribute("login"));
        return modelAndView;
    }

    @GetMapping("/login")
    private ModelAndView login(){
        return new ModelAndView("login");
    }

    @PostMapping("/login")
    private ModelAndView login(@RequestParam("login") String userLogin, HttpSession session){
        ModelAndView modelAndView = new ModelAndView();
        session.setAttribute("login", userLogin);
        modelAndView.setViewName("redirect:home");
        return modelAndView;
    }

    @GetMapping("/logout")
    private ModelAndView logout(HttpServletRequest request){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("redirect:login");
        request.getSession().invalidate();
        return modelAndView;
    }
}
