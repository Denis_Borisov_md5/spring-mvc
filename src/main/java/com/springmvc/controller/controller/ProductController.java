package com.springmvc.controller.controller;

import com.springmvc.entity.Product;
import com.springmvc.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProductController {

    @Autowired
    private ProductService productService;


    @GetMapping("/products")
    private ModelAndView products(){
        ModelAndView modelAndView = new ModelAndView("products");
        modelAndView.addObject("products", productService.getAllProducts());
        return modelAndView;
    }

    @GetMapping("/delete-product/{productId}")
    private ModelAndView deleteProductById(@PathVariable long productId){
        productService.deleteProductById(productId);
        return new ModelAndView("redirect:/products");
    }

    @GetMapping("/create-product")
    private ModelAndView createProduct(){
        return new ModelAndView("create-product");
    }

    @PostMapping("/create-product")
    private ModelAndView createProduct(Product product){
        productService.addProduct(product);
        return new ModelAndView("redirect:/products");
    }

    @GetMapping("/update-product/{productId}")
    private ModelAndView viewUpdateProduct(@PathVariable long productId){
        ModelAndView modelAndView = new ModelAndView("update-product");
        modelAndView.addObject(productService.getProductById(productId));
        return modelAndView;
    }

    @PostMapping("/update-product")
    private ModelAndView updateProduct(Product product){
        productService.updateProduct(product);
        return new ModelAndView("redirect:/products");
    }

    @GetMapping("/search")
    private ModelAndView viewSearch(){
        return new ModelAndView("search");
    }

    @PostMapping("/search")
    private ModelAndView search(@RequestParam("brand") String brand){
        ModelAndView modelAndView = new ModelAndView("products");
        modelAndView.addObject("products", productService.getProductsByBrand(brand));
        return modelAndView;
    }

}
