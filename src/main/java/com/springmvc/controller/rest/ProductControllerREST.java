package com.springmvc.controller.rest;

import com.springmvc.entity.Product;
import com.springmvc.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/products-rest")
@RequiredArgsConstructor
public class ProductControllerREST {

    private final ProductService productService;

    @GetMapping
    private List<Product> productList(){
        return productService.getAllProducts();
    }

    @GetMapping("/{productId}")
    private Product getProduct(@PathVariable long productId) {
        return productService.getProductById(productId);
    }

    @PostMapping("/createProduct")
    private void createProduct(@PathVariable Product product) {
        productService.addProduct(product);
    }

    @PutMapping("/updateProduct/{productId}")
    private void updateProduct(@PathVariable Product product) {
        productService.updateProduct(product);
    }

    @DeleteMapping(value = "/deleteProduct/{productId}")
    private void deleteProduct(@PathVariable long productId) {
        productService.deleteProductById(productId);
    }
}
