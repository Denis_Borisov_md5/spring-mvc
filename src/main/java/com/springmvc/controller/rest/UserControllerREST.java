package com.springmvc.controller.rest;

import com.springmvc.entity.User;
import com.springmvc.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/users-rest")
@RequiredArgsConstructor
public class UserControllerREST {

    private final UserService userService;

    @GetMapping
    private List<User> userList(){
        return userService.getAllUsers();
    }

    @GetMapping("/{userId}")
    private User getUserById(@PathVariable long userId) {
        return userService.getUserById(userId);
    }

    @PostMapping("/createUser")
    private void createUser(@PathVariable User user) {
        userService.addUser(user);
    }

    @PutMapping("/updateUser/{userId}")
    private void updateUser(@PathVariable User user) {
        userService.updateUser(user);
    }

    @DeleteMapping(value = "/deleteUser/{userId}")
    private void deleteUser(@PathVariable long userId) {
        userService.deleteUserById(userId);
    }
}
