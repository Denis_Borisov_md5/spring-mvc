package com.springmvc.service.impl;

import com.springmvc.entity.Product;
import com.springmvc.repository.ProductRepository;
import com.springmvc.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;

    @Override
    public void addProduct(Product product) {
        productRepository.addProduct(product);
    }

    @Override
    public Product getProductById(long productId) {
        return productRepository.getProductById(productId);
    }

    @Override
    public List<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }

    @Override
    public void updateProduct(Product product) {
        productRepository.updateProduct(product);
    }

    @Override
    public void deleteProductById(long productId) {
        productRepository.deleteProductById(productId);
    }

    @Override
    public List<Product> getProductsByBrand(String brand) {
        return productRepository.getAllProducts().stream().
                filter(product -> product.getBrand().contains(brand)).
                collect(Collectors.toList());
    }

}
