package com.springmvc.service.impl;

import com.springmvc.entity.User;
import com.springmvc.repository.UserRepository;
import com.springmvc.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public void addUser(User user) {
        userRepository.addUser(user);
    }

    @Override
    public User getUserById(long userId) {
        return userRepository.getUserById(userId);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }

    @Override
    public void updateUser(User user){
        userRepository.updateUser(user);
    }

    @Override
    public void deleteUserById(long userId) {
        userRepository.deleteUserById(userId);
    }
}
