package com.springmvc.service;

import com.springmvc.entity.Product;

import java.util.List;

public interface ProductService {

    void addProduct(Product product);

    Product getProductById(long productId);

    List<Product> getAllProducts();

    void updateProduct(Product product);

    void deleteProductById(long productId);

    List<Product> getProductsByBrand(String brand);

}
