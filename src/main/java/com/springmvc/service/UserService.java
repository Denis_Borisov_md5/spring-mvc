package com.springmvc.service;

import com.springmvc.entity.User;

import java.util.List;

public interface UserService {

    void addUser (User user);

    User getUserById(long userId);

    List<User> getAllUsers();

    void updateUser(User user);

    void deleteUserById(long userId);

}
