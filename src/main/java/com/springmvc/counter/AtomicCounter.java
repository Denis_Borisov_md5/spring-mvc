package com.springmvc.counter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicCounter {

    public static AtomicInteger userCount = new AtomicInteger();
    public static AtomicInteger productCount = new AtomicInteger();

    public static AtomicInteger getUserCount() {
        return userCount;
    }

    public static void setUserCount(AtomicInteger userCount) {
        AtomicCounter.userCount = userCount;
    }

    public static AtomicInteger getProductCount() {
        return productCount;
    }

    public static void setProductCount(AtomicInteger productCount) {
        AtomicCounter.productCount = productCount;
    }
}
