package com.springmvc.repository.impl;

import com.springmvc.entity.User;
import com.springmvc.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
@Primary
@RequiredArgsConstructor
public class UserRepositoryHibernateImpl implements UserRepository {

    private static final Logger logger = LogManager.getLogger(UserRepositoryHibernateImpl.class);

    private final SessionFactory sessionFactory;

    @Override
    public void addUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(user);
    }

    @Override
    public User getUserById(long userId) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.load(User.class, userId);
    }

    @Override
    public List<User> getAllUsers() {
        Session session = this.sessionFactory.getCurrentSession();
        return (List<User>) session.createQuery("from User").list();
    }

    @Override
    public void updateUser(User user) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(user);
    }

    @Override
    public void deleteUserById(long userId) {
        Session session = this.sessionFactory.getCurrentSession();
        User userToDelete = session.load(User.class, userId);
        if(userToDelete != null){
            session.delete(userToDelete);
        }else {
            logger.error("Deleting user not found");
        }
    }
}
