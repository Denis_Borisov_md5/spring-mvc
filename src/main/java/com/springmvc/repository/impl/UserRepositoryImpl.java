package com.springmvc.repository.impl;

import com.springmvc.entity.User;
import com.springmvc.repository.UserRepository;
import com.springmvc.repository.mapper.UserRowMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class UserRepositoryImpl implements UserRepository {

    private static final String ADD_USER_SQL = "INSERT INTO user (login, password, email)" + "VALUES (?, ?, ?)";
    private static final String GET_USER_SQL = "SELECT * FROM user WHERE userId = ?";
    private static final String GET_ALL_USERS_SQL = "SELECT * FROM user";
    private static final String UPDATE_USER_SQL = "UPDATE user SET login = ?, password = ?, email = ? WHERE userId = ?";
    private static final String DELETE_USER_SQL = "DELETE FROM user WHERE userId = ?";

    private final JdbcTemplate jdbcTemplate;

    @Override
    public void addUser(User user) {
        jdbcTemplate.update(ADD_USER_SQL, user.getLogin(), user.getPassword(), user.getEmail());
    }

    @Override
    public User getUserById(long userId) {
        return jdbcTemplate.queryForObject(GET_USER_SQL, new Object[]{userId}, new UserRowMapper());
    }

    @Override
    public List<User> getAllUsers() {
        return jdbcTemplate.query(GET_ALL_USERS_SQL, new UserRowMapper());
    }

    @Override
    public void updateUser(User user){
        jdbcTemplate.update(UPDATE_USER_SQL, user.getLogin(), user.getPassword(),
                user.getEmail(), user.getUserId());
    }

    @Override
    public void deleteUserById(long userId) {
        jdbcTemplate.update(DELETE_USER_SQL, userId);
    }
}
