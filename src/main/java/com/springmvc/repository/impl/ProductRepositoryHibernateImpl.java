package com.springmvc.repository.impl;

import com.springmvc.entity.Product;
import com.springmvc.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
@Primary
@RequiredArgsConstructor
public class ProductRepositoryHibernateImpl implements ProductRepository {

    private static final Logger logger = LogManager.getLogger(ProductRepositoryHibernateImpl.class);

    private final SessionFactory sessionFactory;

    @Override
    public void addProduct(Product product) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(product);
    }

    @Override
    public Product getProductById(long productId) {
        Session session = this.sessionFactory.getCurrentSession();
        return session.load(Product.class, productId);
    }

    @Override
    public List<Product> getAllProducts() {
        Session session = this.sessionFactory.getCurrentSession();
        return (List<Product>) session.createQuery("from Product").list();
    }

    @Override
    public void updateProduct(Product product) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(product);
    }

    @Override
    public void deleteProductById(long productId) {
        Session session = this.sessionFactory.getCurrentSession();
        Product productToDelete = session.load(Product.class, productId);
        if(productToDelete != null){
            session.delete(productToDelete);
        }else {
            logger.error("Deleting product not found");
        }
    }
}
