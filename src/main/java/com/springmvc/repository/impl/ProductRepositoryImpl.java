package com.springmvc.repository.impl;

import com.springmvc.entity.Product;
import com.springmvc.repository.ProductRepository;
import com.springmvc.repository.mapper.ProductRowMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class ProductRepositoryImpl implements ProductRepository {

    private static final String ADD_PRODUCT_SQL = "INSERT INTO product (brand, name, cost)" + "VALUES (?, ?, ?)";
    private static final String GET_PRODUCT_SQL = "SELECT * FROM product WHERE productId = ?";
    private static final String GET_ALL_PRO_SQL = "SELECT * FROM product";
    private static final String UPDATE_PROD_SQL = "UPDATE product SET brand = ?, name = ?, cost = ? WHERE productId = ?";
    private static final String DELETE_PROD_SQL = "DELETE FROM product WHERE productId = ?";

    private final JdbcTemplate jdbcTemplate;

    @Override
    @Cacheable("products")
    public List<Product> getAllProducts() {
        return jdbcTemplate.query(GET_ALL_PRO_SQL, new ProductRowMapper());
    }

    @Override
    @CacheEvict("products")
    public void addProduct(Product product) {
        jdbcTemplate.update(ADD_PRODUCT_SQL, product.getBrand(), product.getName(), product.getCost());
    }

    @Override
    public Product getProductById(long productId) {
        return jdbcTemplate.queryForObject(GET_PRODUCT_SQL, new Object[]{productId}, new ProductRowMapper());
    }

    @Override
    @CacheEvict("products")
    public void updateProduct(Product product) {
        jdbcTemplate.update(UPDATE_PROD_SQL, product.getBrand(), product.getName(),
                product.getCost(), product.getProductId());
    }

    @Override
    @CacheEvict("products")
    public void deleteProductById(long productId) {
        jdbcTemplate.update(DELETE_PROD_SQL, productId);
    }
}
