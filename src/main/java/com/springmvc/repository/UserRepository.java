package com.springmvc.repository;

import com.springmvc.entity.User;

import java.util.List;

public interface UserRepository {

    void addUser(User user);

    User getUserById(long userId);

    List<User> getAllUsers();

    void updateUser(User user);

    void deleteUserById(long userId);

}
