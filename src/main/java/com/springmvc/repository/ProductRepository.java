package com.springmvc.repository;

import com.springmvc.entity.Product;

import java.util.List;

public interface ProductRepository {

    void addProduct(Product product);

    Product getProductById(long productId);

    List<Product> getAllProducts();

    void updateProduct(Product product);

    void deleteProductById(long productId);

}
