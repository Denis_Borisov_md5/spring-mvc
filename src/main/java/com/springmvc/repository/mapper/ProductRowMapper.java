package com.springmvc.repository.mapper;

import com.springmvc.entity.Product;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductRowMapper implements RowMapper<Product> {

    @Override
    public Product mapRow(ResultSet resultSet, int rowNum) throws SQLException{
        Product product = new Product();
        product.setProductId(resultSet.getLong("productId"));
        product.setBrand(resultSet.getString("brand"));
        product.setName(resultSet.getString("name"));
        product.setCost(resultSet.getLong("cost"));
        return product;
    }
}
