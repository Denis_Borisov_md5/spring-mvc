package com.springmvc.entity;

import lombok.Data;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;


@Entity
@Table(name = "user")
@Proxy(lazy = false)
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class User {

    @Id
    @Column
    @GeneratedValue
    private long userId;

    @Column
    private String login;

    @Column
    private String password;

    @Column
    private String email;
}
