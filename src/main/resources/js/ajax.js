$(document).ready(function () {
    $("#productTable").on("click", "#ajax", function() {
        var $call = $(this);
        var productId = $call.closest('tr').find("td:first").text();
        var durl = '/delete-product/' + productId;
        $.ajax({
            type: 'GET',
            url: durl
        }).done(
            function (answer) {
                $call.closest('tr').remove();
            }
        )
    })
})